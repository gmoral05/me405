## @file main.py
#  Brief doc for main.py
#
#  Detailed doc for main.py 
#
#  @author Giselle Morales
#
#  @date January 7, 2021
#
#

import motor

## A motor driver object
moto = motor.MotorDriver()

moto.set_duty_cycle(50)