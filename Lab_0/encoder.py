## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Giselle Morales
#
#  @copyright License Info
#
#  @date January 7, 2021
#
#  @package encoder
#  Brief doc for the encoder module
#
#  Detailed doc for the encoder module
#
#  @author Giselle Morales
#
#  @copyright License Info
#
#  @date January 7, 2021

import pyb

## An encoder driver object
#
#  Details
#  @author Giselle Morales
#  @copyright License Info
#  @date January 7, 2021
class Encoder:

	## Constructor for encoder driver
	#
	#  Detailed info on encoder driver constructor
	def __init__(self):
		pass

	## Gets the encoder's position
	#
	#  Detailed info on encoder get_position method
	def get_position(self):
		pass

	## Zeros out the encoder
	#
	#  Detailed info on encoder zero function
	def zero(self):
		pass