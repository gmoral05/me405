## @file motor.py
#  Brief doc for motor.py
#
#  Detailed doc for motor.py 
#
#  @author Giselle Morales
#
#  @date January 7, 2021
#
#  @package motor
#  Brief doc for the motor module
#
#  Detailed doc for the motor module
#
#  @author Giselle Morales
#
#  @date January 7, 2021

import pyb

## A motor driver object
#
#  Details
#  @author Your Name
#  @copyright License Info
#  @date January 7, 2021
class Motor:

	## Constructor for motor driver
	#
	#  Detailed info on motor driver constructor
	def __init__(self):
		pass

	## Sets duty cycle for motor
	#
	#  Detailed info on motor driver duty cycle function
	#
	#  @param level The desired duty cycle
	def set_duty_cycle(self,level):
		pass