# -*- coding: utf-8 -*-
"""
@file               getChange.py
@author             Giselle Morales

Created on Wed Jan  6 09:56:29 2021

"""
import math
def getChange(price, payment):
    ''''
    @brief              This function calculates the correct change for a purchase.
    @details            This file takes in an tuple represented by 'payment' and
                        price which represents the price of the item in cents.
                        The price is also an integer and this program will dismiss
                        non-integers. The function in this file (getChange) returns
                        any remaining change in the fewest denominations possible. 
    @param price        A floating point value that represents the price of an item.
    @param payment      A tuple representing the integer number of pennies, nickles,
                        dimes, quarters, ones, fives, tens, and twenties.
    '''

    # Check if payment/price is integer. Reject strings, etc.   
    for item in payment:
            if not isinstance(item, int):
                return None
    else:
        pass
    if price > 0:
        pass
    else:
        return None
    
        # Add up denominations in tuple inputted.
        # Payment tuple [pennies, nickles, dimes, quarters, ones, fives, tens, twenties]
    input_payment= int(payment[0]*1 + payment[1]*5 + payment[2]*10 
                       + payment[3]*25 + payment[4]*100 + payment[5]*500 
                       + payment[6]*1000 + payment[7]*2000)

    # Check if payment is negative. Rejects if negative values are entered.
    if input_payment < 0:
        # print ('Invalid payment')
        return None
    else:
        pass

    # Check to see if payment is sufficent. (Round down)
    if input_payment>= price:
        total_change= int(input_payment- price)

        # Return least number of denominations. Starting with largest value and
        # working down to the smallest denomination ensures least amount of 
        # change is given back
        twent= math.floor(total_change/2000) #20's
        # print('TWENTY: ' + str(twent))
        total_change= round(total_change- twent*2000)
        # print('New Change: ' + str(total_change))
        
        tens= math.floor(total_change/1000) #10's
        # print ('TENS: ' + str((tens)))
        total_change= round(total_change- tens*1000)
        # print('New Change: ' +str(total_change))
        
        fiv= math.floor(total_change/500) #5's
        # print ('FIVES: ' + str((fiv)))
        total_change= round(total_change- (fiv*500))
        
        ones= math.floor(total_change/100) #1's
        # print ('ONES: ' + str(round(tens)))
        total_change= round(total_change- (ones*100))
        
        quart= math.floor(total_change/25) #0.25
        total_change= round(total_change- (quart*25))
        
        dim= math.floor(total_change/10) #0.1
        total_change= round(total_change- (dim*10))
        
        nick= math.floor(total_change/5) #0.05
        total_change= round(total_change- (nick*5))
        
        penn= math.floor(total_change/1) #0.01
        total_change= round(total_change- penn)
        
        # Format change tuple (Round down)
        change= (round(penn), round(nick), round(dim), round(quart), round(ones), round(fiv), round(tens), round(twent))
        return change
        # print ('Here is your change: ' + str(change))
        
    else:
        # print ('Not enough funds. Please insert more money.')
        return None


if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if__name__== '__main__' block will only run when the script
    # is executed as a standalone program. If the script is imported as a module the
    # code block will not run
    
    # Execute the function
    price= 1000 # 10 dollars
    payment = (0, 0, 0, 4, 0, 0, 1, 0)
    #       (.01,.05,.1,.25,1,5,10,20)
    getChange(price, payment)

    
    
    