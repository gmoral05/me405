# -*- coding: utf-8 -*-
"""
@file LAB0x02.py
@brief      Uses external interrupts to test user reaction time
@details    This script uses interrupts to measure the reaction time of a user. The program begins by printing a message informing the user how to play the game. When the user
            is reacy, they press the button, and the game begins. The program waits a random time between 2 and 3 seconds, then turns on the LED light on the microcontroller.
            The LED is only on for 1 second. If the user presses the button within the timeframe, their time is saved in a list and a message is printed informing them of their 
            time. If the user doesn't press the button within the allocated time, the program informs them to try again. When the program is properly exited by pressing ctrl-C,
            the average score of the user is printed and the program exits.
@author     Giselle Morales
@date       February 4th, 2020

"""

import pyb
import utime
from pyb import ExtInt
import random


def button_press(which_pin):# Create an interrupt service routine
        '''
        @brief      Create an interrupt service routine
        @details    When this interrupt is called out in the script (aka when the button is pressed), the button would read as true and will
                    allow the code to function with this interruption.
                    
        '''
    # Global variable for button pressing
        global isr_press
        if isr_press!= True: #Does not equal
            isr_press= True
        elif isr_press == True:
            pass
    
## Initialize blue button C13 from the Nucleo board with interrupt on falling edge
extint = pyb.ExtInt (pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, button_press)                   # Interrupt service routine
isr_press= False
## Set up timer (32-bit), timer 2. 
tim= pyb.Timer(2, prescaler= 83, period=0x7FFFFFFF)
## Set up LED
led= pyb.Pin(pyb.Pin.cpu.A5)
## Set up start time in microseconds 
starttim= tim.counter()
## Set up current time in microseconds 
currTime= tim.counter()
## Set up updated time in microseconds 
update= currTime- starttim
## Set up variable to print initializing method once
print_= True
## Store and append reaction times
reac= []

# Initiate with print command and begin
if print_== True:
    print('Test your reaction time! When ready, press the blue button. When the LED turns on, the timer starts.')
    print_=False
    isr_press= False
# Begin the sequence
while True:
    try:
        if isr_press:
            # Waits for 2 to 3 seconds
            utime.sleep_ms(random.randint(2000, 3000))
            # Turn on lED
            isr_press = False
            print('Press!')
            led.high()
            begin= tim.counter()
            curr= tim.counter()
        
            if (curr - begin) < 1000000: # In under 1 seconds
                # Update current time
                curr= tim.counter()
                if isr_press:
                    # If the button has been pressed again
                    result= curr- begin
                    reac.append(result)
                    print ('Here is your reaction time: ' + str (result) + 'ms')
                    led.low()

        
            if isr_press== False:
                # If the button was never pressed within 1 second
                print ('Timed out. Resetting. Please try again.')
                led.low()
                
        # Reset blue button and the initializing message
        isr_press= False
        print_= True
        if len(reac) > 0:
            average= sum(reac)/ len(reac)
        
    except KeyboardInterrupt:
        print ('Average score: ' + str(average))
        break
    
    