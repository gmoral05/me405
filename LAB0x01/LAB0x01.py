# -*- coding: utf-8 -*-
"""
@file               LAB0x01.py
@brief              This file acts as a virtual vending machine.
@details            This file works in conjunction with keyboard to detect and
                    read user inputs. Here, the vending machine prints an inital
                    statment which outlines the items, prices, and buttons to use
                    when selecting a beverage. In turn, the machine calulcates
                    the amount of money inputted and the amount of money
                    spent. When the user makes their selection, the beverage is
                    dispensed.
@page page_LAB1     LAB0x01: Vending Machine
@author             Giselle Morales
"""
state = 0

import math
import keyboard
pushed_key = None

def getChange(price, payment):
    """ Computes change
    """
    ''''
    @brief              This function calculates the correct change for a purchase.
    @details            This file takes in an tuple represented by 'payment' and
                        price which represents the price of the item in cents.
                        The price is also an integer and this program will dismiss
                        non-integers. The function in this file (getChange) returns
                        any remaining change in the fewest denominations possible. 
    @param price        A floating point value that represents the price of an item.
    @param payment      A tuple representing the integer number of pennies, nickles,
                        dimes, quarters, ones, fives, tens, and twenties.
    '''

    # Check if payment/price is integer. Reject strings, etc.   
    for item in payment:
            if not isinstance(item, int):
                return None
    else:
        pass
    if price >= 0:
        pass
    else:
        return None
    
        # Add up denominations in tuple inputted.
        # Payment tuple [pennies, nickles, dimes, quarters, ones, fives, tens, twenties]
    input_payment= int(payment[0]*1 + payment[1]*5 + payment[2]*10 
                       + payment[3]*25 + payment[4]*100 + payment[5]*500 
                       + payment[6]*1000 + payment[7]*2000)

    # Check if payment is negative. Rejects if negative values are entered.
    if input_payment < 0:
        # print ('Invalid payment')
        return None
    else:
        pass

    # Check to see if payment is sufficent. (Round down)
    if input_payment>= price:
        total_change= int(input_payment- price)

        # Return least number of denominations. Starting with largest value and
        # working down to the smallest denomination ensures least amount of 
        # change is given back
        twent= math.floor(total_change/2000) #20's
        total_change= round(total_change- twent*2000)
        
        tens= math.floor(total_change/1000) #10's
        total_change= round(total_change- tens*1000)
        
        fiv= math.floor(total_change/500) #5's
        total_change= round(total_change- (fiv*500))
        
        ones= math.floor(total_change/100) #1's
        total_change= round(total_change- (ones*100))
        
        quart= math.floor(total_change/25) #0.25
        total_change= round(total_change- (quart*25))
        
        dim= math.floor(total_change/10) #0.1
        total_change= round(total_change- (dim*10))
        
        nick= math.floor(total_change/5) #0.05
        total_change= round(total_change- (nick*5))
        
        penn= math.floor(total_change/1) #0.01
        total_change= round(total_change- penn)
        
        # Format change tuple (Round down)
        change= (round(penn), round(nick), round(dim), round(quart), round(ones), round(fiv), round(tens), round(twent))
        return change
        # print ('Here is your change: ' + str(change))
        
    else:
        # print ('Not enough funds. Please insert more money.')
        return None
    pass

def totalBalance(in_payment):
    ''''
    @brief              This function calculates the total payment input.
    @details            This file takes in an tuple represented by 'in_payment'.
                        The total input payment in than calulcated and kept 
                        stored in the tuple which is later returned.
    @param in_payment   A tuple representing the integer number of pennies, nickles,
                        dimes, quarters, ones, fives, tens, and twenties.
    '''

    return float(in_payment[0]*1 + in_payment[1]*5 + in_payment[2]*10 
                       + in_payment[3]*25 + in_payment[4]*100 + in_payment[5]*500 
                       + in_payment[6]*1000 + in_payment[7]*2000)

def printWelcome():
    ''''
    @brief              This function prints the machines menu.
    @details            This piece of code prints a detailed menu of what is 
                        offered to buy in the vending machine. Inputs are 
                        outlined in the print statements. 
    '''

    print('')
    print('Hello from Vendotron^TM! Here are the beverage prices: ')
    beverages= ['Cuke= $1.00','Popsi= $1.20','Spryte= $0.85','Dr.Pupper= $1.10']
    print(*beverages, sep='\n')
    print('')
    print('Input Payment as follows:' )
    cost= ['0 for $0.01', '1 for $0.05', '1 for $0.10', '2 for $0.25', '4 for $1.00', '5 for $5.00', '6 for $10.00', '7 for $20.00']
    print(*cost, sep='\n')
    print('')
    print('Enter following characters for Drink Selection: ')
    beverages= ['"c"= Cuke','"p"= Popsi','"s"= Spryte','"d"= Dr.Pupper']
    print(*beverages, sep='\n')
    
    state== 0
    keyboard.on_press (on_keypress)
    pass

def on_keypress (thing):
    ''''
    @brief              This function facilitates keyboard inputs.
    @details            This function prints, returns, and calulcates the 
                        prices and payment totals of the drinks inside the
                        vending machine. Payment inputted is being summed but
                        when a drink selection is made, the change is calulcated
                        and the machine vends the item. 
    @param thing        An internal command to detect keyboard pressing.
    '''

    global pushed_key

    pushed_key = thing.name

while True:
    """Implement FSM using a while loop and an if statement
        will run eternally until user presses CTRL-C
    """
    penn  =  0
    nick  =  0
    dim   =  0
    quart =  0
    ones  =  0
    fiv   =  0
    tens  =  0
    twent =  0
    
    if state == 0:
        """perform state 0 operations
        this init state, initializes the FSM itself, with all the
        code features already set up
        """
        printWelcome() 
        in_payment = [0, 0, 0, 0, 0, 0, 0, 0]
        state = 1 # on the next iteration, the FSM will run state 1
    
    elif state == 1:
        """perform state 1 operations
        """

        if pushed_key:
                    if pushed_key == "0": #0.01
                        in_payment[0] +=1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
                        
                    elif pushed_key == "1": #0.05
                        in_payment[1] += 1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
                        
                    elif pushed_key == "2": #0.10
                        in_payment[2] += 1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
                        
                    elif pushed_key == "3": #0.25
                        in_payment[3] += 1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
            
                    elif pushed_key == "4": #1.00
                        in_payment[4] += 1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
                        
                    elif pushed_key == "5": #5.00
                        in_payment[5] += 1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
                
                    elif pushed_key == "6": #10.00
                        in_payment[6] += 1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
                        
                    elif pushed_key == "7": #20.00
                        in_payment[7] += 1
                        # LCD= getChange(1, in_payment)
                        print ('Balance: ' + str(in_payment))
                        
                    elif pushed_key == "e":
                        state= 3
                        print ('Ejected. Goodbye!') 
                        
                    elif pushed_key == "c":
                        cost= 100
                        state= 2
                        print('')
                        print ('Cuke Selected!') 
                        
                    elif pushed_key == "p":
                        cost= 120
                        state= 2
                        print('')
                        print ('Popsi Selected!') 
        
                    elif pushed_key == "s":
                        cost= 85
                        state= 2
                        print('')
                        print ('Spryte Selected!') 
                    
                    elif pushed_key == "d":
                        cost= 110
                        state= 2
                        print('')
                        print ('Dr. Pupper Selected!') 
                        
                    pushed_key= None
        # except KeyboardInterrupt:
        #         break
        
        # state = 2 # s1 -> s2
    
    elif state == 2:
        """Check if funds are sufficent. If not, return to state 1. If so, transition
        to state 3.
        """
        if totalBalance(in_payment)>= cost:
            in_payment= getChange(cost, in_payment)
            print ('Dispensing Beverage.....')
            print ('Remaining Balance: ' +str(in_payment))
            print ('Press "e" to dispense your change or Make another selection')
            print ('')
        elif totalBalance(in_payment) < cost:
            print ('')
            print ('Insufficent. Press "e" to cancel and restart your transaction.')
            print ('')
        state = 1 # s2 -> s3
    
    elif state == 3:
        """perform state 3 operations
        """
        change= getChange(0, in_payment)
        print ('Your change is: ' + str(change))
        print('')
        state= 0# s3 -> s1
    
    else:
        """Take care of error
        """
        print('Please press e and continue with your transaction')
    pass


    